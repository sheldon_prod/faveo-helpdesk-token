<div class="row">

  @if((sizeof($files) > 0) || (sizeof($directories) > 0))

  @foreach($directories as $key => $dir_name)
  @include('laravel-filemanager::folders')
  @endforeach

  @foreach($files as $key => $file)
  @include('laravel-filemanager::item')
  @endforeach

  @else
  <div class="col-md-12">
    <p>{{ Lang::get('laravel-filemanager::lfm.message-empty') }}</p>
  </div>
  @endif

</div>
